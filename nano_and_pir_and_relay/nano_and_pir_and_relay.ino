#include <SPI.h>
#include <SimpleTimer.h>

int d2 = 2; // pin PIR
int d6 = 6; // Relay 1
String lightTime = "900"; // Period when relay is on, 15min


unsigned long currentMillis = 0;
unsigned long previousMillis = 0;
unsigned long pTimerLightMillis = 0;

SimpleTimer timer;

void setup() {
  Serial.begin(9600);
  delay(2000);
  
  Serial.println("Start: ");

  delay(1000);
  
  pinMode(d2, INPUT);
  pinMode(d6, OUTPUT);
  
  digitalWrite(d6,HIGH);
}

void loop() {
  currentMillis = millis();
  timer.run(); // Initiates SimpleTimer

  Serial.print("currentMillis: ");
  Serial.println(currentMillis);
  Serial.print("pTimerLightMillis: ");
  Serial.println(pTimerLightMillis);
  Serial.print("d2: ");
  Serial.println(digitalRead(d2));
  Serial.print("d6: ");
  Serial.println(digitalRead(d6));

  mooveDetected();
  if((currentMillis-pTimerLightMillis) > lightTime.toInt()*1000){ // Turn OFF relay after timещut
    lightSwitchOff();
  }
}

void mooveDetected(){
  if(digitalRead(d2) == HIGH){
    lightSwitchOn();
  }
}

void lightSwitchOn(){ // Relay ON
  digitalWrite(d6,LOW);
  pTimerLightMillis = currentMillis;
}
void lightSwitchOff(){ // Relay OFF
  digitalWrite(d6,HIGH);
}
